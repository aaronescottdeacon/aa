// Aritmetic Operators
// console.log(5 + 5);
// console.log(5 * 10);
// console.log(10 % 3);
// console.log(5 + 10 / 2 * 5 - 10);
// console.log((6 + 10) / 2 * 5 - 10);

// result-
// 10, 50, 1, 20, 30

// Assignment Operators
// x=0
// console.log(x = x + 1);
// console.log(x+= 1);
// console.log(x++);
// console.log(++x);

// result-
// 1, 2, 2, 4

// Relational Operators
// console.log(5 > 3);
// console.log(3 != 3);
// console.log(3 <= 2 && 5 >2);
// console.log(!5>3);

// result-
// true, false, false, false

// Mismatched Types
// console.log(5 + "5");
// console.log(5 + true);
// console.log(5 * "5");
// console.log(1 == true);
// console.log(1 === true);

// result-
// 55, 6, 25, true, false

