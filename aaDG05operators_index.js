// Aritmetic Operators
// console.log(5 + 5);
// console.log(5 * 10);
// console.log(10 % 3);
// console.log(5 + 10 / 2 * 5 - 10);
// console.log((6 + 10) / 2 * 5 - 10);

// Assignment Operators
// console.log(x = x + 1);
// console.log(x+= 1);
// console.log(x++);
// console.log(++x);

// Relational Operators
// console.log(5 > 3);
// console.log(3 != 3);
// console.log(3 <= 2 && 5 >2);
// console.log(!5>3);

// Mismatched Types
// console.log(5 + "5");
// console.log(5 + true);
// console.log(5 * "5");
// console.log(1 == true);
// console.log(1 === true);

// let a = 2;
// let b = "two";
// let c = "2";
// alert(typeof a);// alerts "number"
// alert(typeof b);// alerts "string"
// alert(typeof c);// alerts "string"

// alert(a * a);// alerts 4
// alert(a + b);// alerts 2two
// alert(a * c);// alerts 4
// alert(typeof (a * a));// alerts "number"
// alert(typeof (a + b));// alerts "string"
// alert(typeof (a * c));// alerts "number"

// var a = true; var b = 1;
// alert(a == b); // ???
// alert(a === b); // ???
// alert(a != b); // ???
// alert(a !== b); // ???

// let s = "5"; this section created an error
// let i = 5;
// let total = i + parseint(s); //returns 10 not 55

// isNaN(s); // returns true
// !isNaN(i); //returns true

console.log(5 + 5);	
console.log(5 * 10);	
console.log(10 % 3);	
console.log(5 + 10 / 2 * 5 - 10);	
console.log((6 + 10) / 2 * 5 - 10);	
